# Connective Tissue

Connective tissue in iMSTK is represented with clusters of segments using a `LineMesh` geometry. It is then simualted with position-based-dynamics using distance and bend constraints.

